require('@babel/register')({
  ignore: ['node_modules', '.next'],
  presets: ['@babel/preset-env'],
})

// Import the rest of our application.
module.exports = require('./server')
