/* eslint-disable no-console */
require('dotenv').config()

const path = require('path')
const https = require('https')
const http = require('http')
const fs = require('fs')

const express = require('express')
const compression = require('compression')
const next = require('next')
const helmet = require('helmet')
const nextI18NextMiddleware = require('next-i18next/middleware').default

const nextI18next = require('../src/libs/i18n')
const routes = require('../routes')
const hostname = process.env.HOST || 'localhost'
const port = parseInt(process.env.PORT, 10) || 3100
const isHttps = process.env.HTTPS || false
const dev = process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'staging'
const app = next({ dev })

const handler = routes.getRequestHandler(app)

app.prepare().then(() => {
  const app = express()

  app.use(nextI18NextMiddleware(nextI18next))
  app.use(helmet())
  app.use(compression())

  const staticPath = path.join(__dirname, '../public/static')
  app.use(
    '/public/static',
    express.static(staticPath, {
      immutable: true,
      maxAge: '30d',
    })
  )

  app.get('*', (req, res) => {
    return handler(req, res)
  })

  startServer()

  function startServer() {
    function init() {
      if (isHttps) {
        return https.createServer(
          {
            cert: fs.readFileSync(process.env.SSL_CERT),
            key: fs.readFileSync(process.env.SSL_PRIVATE_KEY),
          },
          app
        )
      } else {
        return http.createServer(app)
      }
    }

    init().listen(port, hostname, (err) => {
      if (err) throw err
      console.log(`> Ready on ${isHttps ? 'https' : 'http'}://${hostname}:${port}`)
    })
  }
})
