# Next.js redux starter

An opinionated Next.js starter kit with Express, Redux, and react-testing-library.

[![Build Status](https://travis-ci.org/CodementorIO/nextjs-redux-starter.svg?branch=master)](https://travis-ci.org/CodementorIO/nextjs-redux-starter)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com) [![Greenkeeper badge](https://badges.greenkeeper.io/CodementorIO/nextjs-redux-starter.svg)](https://greenkeeper.io/)

## About

Next.js is an awesome and minimalistic framework to make a modern universal react app. However, there're times that we need a bit more features to build a complex SPA (single page application). That's why this project is born.

## Features

- 🚄 Dynamic routing with [express](https://github.com/expressjs/express) and [next-routes](https://github.com/fridays/next-routes).
- 🗄 State management with [redux](https://github.com/reactjs/redux), [react-redux](https://github.com/reactjs/react-redux), and [next-redux-wrapper](https://github.com/kirill-konshin/next-redux-wrapper)
- Styling with [@zeit/next-sass]
- 🐐 Unit testing with [react-testing-library](https://github.com/testing-library/react-testing-library)
- 🛀 Linting staged changes on [pre-commit](https://github.com/pre-commit/pre-commit) with [standard](https://github.com/standard/standard)
- ⛑ [react-helmet](https://github.com/nfl/react-helmet), [Immutable.js
  ](https://github.com/facebook/immutable-js/), [dotenv](https://github.com/motdotla/dotenv), and more...

## Getting started

```
git clone https://gitlab.com/CuongCycle/nextjs my-project
cd my-project
yarn
yarn dev
```

Then open `http://localhost:3100/` to see your app.

### Deployment

After `yarn build` finished, run

Run `yarn start`

If you prefer using `now`, just modify `now.json` config.

## Structure overview

```
├── README.md
├── .babelrc
├── .editorconfig
├── .env
├── .eslintrc
├── .gitignore
├── .prettierrc
├── .sass-lint.yml
├── next.config.js
├── now.json
├── package.json
├── routes.js
├── sass-lint-auto-fix.yml
├── start.js
├── yarn.lock
├── pages
│   ├── _app.js
│   ├── _document.js
│   └── index.js
├── src
│   ├── containers
│   │   ├── index.js
│   │   └── HomeContainer
│   │       ├── HomeContainer.js
│   │       └── HomeContainer.scss
│   ├── sections
│   │   ├── index.js
│   │   ├── NameSection
│   │   │   ├── NameSection.js
│   │   │   └── NameSection.scss
│   │   └── NameSection
│   │       ├── NameSection.js
│   │       └── NameSection.scss
│   ├── components
│   │   ├── index.js
│   │   ├── UserCard
│   │   │   ├── UserCard.js
│   │   │   └── UserCard.scss
│   │   └── Layout.js
│   ├── redux
│   │   ├── actionName.js
│   │   ├── actionName.js
│   │   ├── constants.js
│   │   ├── initState.js
│   │   └── reducer.js
│   ├── actions
│   │   └── index.js
│   ├── reducers
│   │   └── index.js
│   ├── store
│   │   └── createStore.js
│   ├── constants
│   │   └── index.js
│   ├── config.js
│   ├── fonts
│   ├── libs
│   │   └──i18n.js
│   ├── styles
│   │   ├── _animation.scss
│   │   ├── _variables.scss
│   │   ├── _mixins.scss
│   │   ├── _fonts.scss
│   │   ├── _forms.scss
│   │   ├── _reset.scss
│   │   ├── global.scss
│   │   └── index.scss
│   └── test
│       ├── sections
│       │   └── NameSection.test.js
│       └── test-utils.js
├── public
│   └── static
│       └── locales
│           ├── en
│           │   └── common.json
│           └── vi
│               └── common.json
└── server
    └── index.js
```

## Workspace setting config

```
"settings": {
  "sassFormat.indent": 2,
  "sassFormat.useSingleQuotes": true,
  "prettier.jsxSingleQuote": true,
  "prettier.singleQuote": true,
  "files.autoSave": "off",
  "javascript.format.enable": false,
  "explorer.confirmDragAndDrop": false,
  "vsicons.projectDetection.autoReload": true
}
```
