const nextBuildId = require('next-build-id')
const withSass = require('@zeit/next-sass')
const withCSS = require('@zeit/next-css')
const { ANALYZE, ASSET_HOST } = process.env
// for those who using CDN
const assetPrefix = ASSET_HOST || ''

module.exports = withCSS(
  withSass({
    assetPrefix,
    env: {
      ENV: process.env.NODE_ENV,
    },
    generateBuildId: async () => nextBuildId({ describe: true, dir: __dirname }),
    target: 'serverless',
    webpack: (config, { dev }) => {
      config.output.publicPath = `${assetPrefix}${config.output.publicPath}`

      if (ANALYZE) {
        const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
        config.plugins.push(
          new BundleAnalyzerPlugin({
            analyzerMode: 'server',
            analyzerPort: 8888,
            openAnalyzer: true,
          })
        )
      }

      config.module.rules.push({
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif|css)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 100000,
          },
        },
      })

      return config
    },
  })
)
