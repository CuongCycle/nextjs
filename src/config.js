export default {
  env: process.env.NODE_ENV,
  githubApiEndpoint: process.env.GITHUB_API_ENDPOINT,
  mode: process.env.MODE,
}
