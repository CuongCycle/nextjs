// import NextI18Next from 'next-i18next'
const NextI18Next = require('next-i18next').default

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: 'vi',
  lng: 'vi',
  otherLanguages: ['en'],
})

module.exports = {
  Link: NextI18NextInstance.Link,
  Trans: NextI18NextInstance.Trans,
  appWithTranslation: NextI18NextInstance.appWithTranslation,
  config: NextI18NextInstance.config,
  default: NextI18NextInstance,
  i18n: NextI18NextInstance.i18n,
  withTranslation: NextI18NextInstance.withTranslation,
}
