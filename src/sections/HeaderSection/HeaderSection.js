import React from 'react'
import Link from 'next/link'
// import PropTypes from 'prop-types';

import Constants from '../../constants'

const HeaderSection = () => {
  return (
    <header className="s-section s-header">
      <div className="b-container">
        <img src="/static/next-logo.png" className="logo" />
        <h1>NextJS Redux Starter!</h1>
        <nav>
          <Link href="/">
            <a>Home Page</a>
          </Link>
          <Link href={Constants.PAGE.USERS}>
            <a>Users</a>
          </Link>
        </nav>
      </div>
    </header>
  )
}

HeaderSection.propTypes = {}

HeaderSection.defaultProps = {}

export default HeaderSection
