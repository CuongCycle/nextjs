import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import '../styles/index.scss'

import Constants from '../constants'
import { withTranslation } from '../libs/i18n'
import HeaderContainer from '../containers/HeaderContainer/HeaderContainer'

class Layout extends PureComponent {
  componentDidMount() {
    require('../../node_modules/css-browser-selector/css_browser_selector')
  }

  render() {
    const { t, host, router, children, className } = this.props
    const { pathname, query } = router
    const layoutProps = {
      className: `app ${className}`,
    }
    const headerContainerProps = {
      host,
      pathname,
      query,
      t,
    }

    return (
      <div {...layoutProps}>
        <HeaderContainer {...headerContainerProps} />
        {children}
      </div>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  host: PropTypes.string,
  policyCateList: PropTypes.object,
  router: PropTypes.object,
  t: PropTypes.func,
}

Layout.defaultProps = {
  children: undefined,
  className: '',
  host: '',
  router: {},
  t: Constants.NOOP,
}

export default withTranslation('common')(Layout)
