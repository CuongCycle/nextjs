import React from 'react'
// import * as R from 'ramda'
import Link from 'next/link'
import PropTypes from 'prop-types'

import Constants from '../../constants'

const UserCard = (props) => {
  const { id, firstName, lastName, email, avatar } = props

  return (
    <div className="c-user-card">
      <Link href={`${Constants.PAGE.USER_DETAIL}/[id]`} as={`${Constants.PAGE.USER_DETAIL}/${id}`}>
        <a>
          <div className="image">
            <figure
              style={{
                backgroundImage: `url('${avatar}')`,
              }}
            />
          </div>
          <p>
            Full Name: {firstName} {lastName}
          </p>
          <p>Email: {email}</p>
        </a>
      </Link>
    </div>
  )
}

UserCard.propTypes = {
  avatar: PropTypes.string,
  email: PropTypes.string,
  firstName: PropTypes.string,
  id: PropTypes.string,
  lastName: PropTypes.string,
}

UserCard.defaultProps = {
  avatar: '',
  email: '',
  firstName: '',
  id: '',
  lastName: '',
}

export default UserCard
