import React from 'react'
// import * as R from 'ramda';
import PropTypes from 'prop-types'

import { UserCard } from '../'

const UsersList = props => {
  const { data } = props

  return (
    <div className='c-users-list'>
      <ul>
        {data.map((user, i) => (
          <li key={i}>
            <UserCard {...user} />
          </li>
        ))}
      </ul>
    </div>
  )
}

UsersList.propTypes = {
  data: PropTypes.array
}

UsersList.defaultProps = {
  data: []
}

export default UsersList
