export default {
  COMMON: {
    HOME: 'home',
  },
  EMPTY: {
    ARRAY: [],
    FUNCTION: () => {},
    OBJECT: {},
    OPTION: { label: '', value: '' },
    PROMISE: () => Promise.resolve(),
  },
  PAGE: {
    USERS: '/users',
    USER_DETAIL: '/user-detail',
  },
}
