// import React from 'react'
import * as R from 'ramda'
// import PropTypes from 'prop-types'

import Constants from '../constants'
// import { i18n } from '../libs/i18n'

export const convertRouterToClassName = ({ pathname = '' } = {}) =>
  R.equals(pathname, '/') ? Constants.COMMON.HOME : R.compose(R.propOr('', [1]), R.split('/'), decodeURI)(pathname)
