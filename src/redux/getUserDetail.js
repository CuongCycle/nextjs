import axios from 'axios'
import humps from 'humps'
import * as R from 'ramda'

import { cAPI } from '../services/api'

import {
  COMMON_GET_USER_DETAIL_BEGIN,
  COMMON_GET_USER_DETAIL_SUCCESS,
  COMMON_GET_USER_DETAIL_FAILURE,
} from './constants'

// import Constants from '../constants';

export const getUserDetail = (id = 0) => (dispatch) => {
  dispatch({
    type: COMMON_GET_USER_DETAIL_BEGIN,
  })

  const promise = new Promise((resolve, reject) => {
    const doRequest = axios.get(`users/${id}`, cAPI)

    doRequest.then(
      (res) => {
        const resData = humps.camelizeKeys(R.propOr({}, 'data')(res))
        const data = R.pathOr([], ['data'])(resData)
        const payload = {
          data,
        }

        dispatch({
          payload,
          type: COMMON_GET_USER_DETAIL_SUCCESS,
        })
        resolve(payload)
      },
      (err) => {
        dispatch({
          payload: { error: err },
          type: COMMON_GET_USER_DETAIL_FAILURE,
        })
        reject(err)
      }
    )
  })

  return promise
}

export const reducer = (state, action) => {
  switch (action.type) {
    case COMMON_GET_USER_DETAIL_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        userDetail: {
          ...state.userDetail,
          error: null,
          pending: true,
        },
      }

    case COMMON_GET_USER_DETAIL_SUCCESS:
      // The request is success
      return {
        ...state,
        userDetail: {
          ...action.payload.data,
          error: null,
          pending: false,
        },
      }

    case COMMON_GET_USER_DETAIL_FAILURE:
      // The request is failed
      return {
        ...state,
        userDetail: {
          ...state.userDetail,
          error: action.payload.error,
          pending: false,
        },
      }

    default:
      return state
  }
}
