// import Constants from '../../constants';

export default {
  userDetail: {
    avatar: '',
    email: '',
    error: null,
    firstName: '',
    id: '',
    lastName: '',
    pending: false,
  },
  usersList: {
    data: [],
    error: null,
    last: false,
    page: 0,
    pending: false,
    total: 0,
  },
}
