import axios from 'axios'
import humps from 'humps'
import * as R from 'ramda'

import { cAPI } from '../services/api'

import { COMMON_GET_USERS_BEGIN, COMMON_GET_USERS_SUCCESS, COMMON_GET_USERS_FAILURE } from './constants'

// import Constants from '../constants';

export const getUsers = ({ page = 1 } = {}) => (dispatch) => {
  dispatch({
    type: COMMON_GET_USERS_BEGIN,
  })

  const promise = new Promise((resolve, reject) => {
    const doRequest = axios.get(`users?page=${page}`, cAPI)

    doRequest.then(
      (res) => {
        const resData = humps.camelizeKeys(R.propOr({}, 'data')(res))
        const page = R.pathOr(0, ['page'])(resData)
        const data = R.pathOr([], ['data'])(resData)
        const total = R.pathOr(0, ['total'])(resData)
        const totalPages = R.pathOr(0, ['totalPages'])(resData)
        const payload = {
          data,
          last: R.gte(page, totalPages),
          page,
          total,
        }

        dispatch({
          payload,
          type: COMMON_GET_USERS_SUCCESS,
        })
        resolve(payload)
      },
      (err) => {
        dispatch({
          payload: { err },
          type: COMMON_GET_USERS_FAILURE,
        })
        reject(err)
      }
    )
  })

  return promise
}

export const reducer = (state, action) => {
  switch (action.type) {
    case COMMON_GET_USERS_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        usersList: {
          ...state.usersList,
          error: null,
          pending: true,
        },
      }

    case COMMON_GET_USERS_SUCCESS:
      // The request is success
      return {
        ...state,
        usersList: {
          ...state.usersList,
          data: R.gt(action.payload.page, 1) ? [...state.usersList.data, ...action.payload.data] : action.payload.data,
          error: null,
          last: action.payload.last,
          page: action.payload.page,
          pending: false,
          total: action.payload.total,
        },
      }

    case COMMON_GET_USERS_FAILURE:
      // The request is failed
      return {
        ...state,
        usersList: {
          ...state.usersList,
          error: action.payload.err,
          pending: false,
        },
      }

    default:
      return state
  }
}
