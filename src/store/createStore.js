/* eslint-disable no-unused-vars */
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import config from '../config'
import rootReducer from '../reducers'

function createMiddlewares({ isServer }) {
  const middlewares = [thunkMiddleware]

  if (config.env === 'development' && typeof window !== 'undefined') {
    middlewares.push(
      createLogger({
        collapsed: true,
        level: 'info',
        stateTransformer: (state) => {
          const newState = {}

          for (const i of Object.keys(state)) {
            newState[i] = state[i]
          }

          return newState
        },
      })
    )
  }

  return middlewares
}

export default (initialState = {}, context) => {
  const { isServer } = context
  const middlewares = createMiddlewares({ isServer })
  const state = initialState

  return createStore(rootReducer, state, composeWithDevTools(applyMiddleware(...middlewares)))
}
