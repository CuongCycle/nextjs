import React, { PureComponent } from 'react'
import * as R from 'ramda'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { withTranslation } from '../../libs/i18n'
import { getUserDetail } from '../../actions'
import { UserCard } from '../../components'

class UsersContainer extends PureComponent {
  static async getInitialProps(ctx) {
    const { store, query } = ctx
    const namespacesRequired = ['common']
    const { id = '' } = query

    const doAllDispatches = (dispatch) => Promise.all([dispatch(getUserDetail(id))])

    try {
      await store.dispatch(doAllDispatches)
      return {
        namespacesRequired,
      }
    } catch (err) {
      console.error(err)

      return {
        namespacesRequired,
      }
    }
  }

  render() {
    const { userDetail } = this.props

    return (
      <>
        <Helmet>
          <title>User Detail</title>
          <meta name="description" content="User Detail" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="User Detail" />
          <meta property="og:description" content="User Detail" />
        </Helmet>
        <div className="s-section s-user-detail">
          <div className="b-container">
            <h1>{R.pathOr('', ['firstName'])(userDetail)}</h1>
            <UserCard {...userDetail} />
          </div>
        </div>
      </>
    )
  }
}

const mapStateToProps = ({ common }) => {
  const { userDetail } = common

  return { userDetail }
}

UsersContainer.propTypes = {
  userDetail: PropTypes.object,
}

UsersContainer.defaultProps = {
  userDetail: {},
}

export default connect(mapStateToProps, {})(withTranslation('common')(UsersContainer))
