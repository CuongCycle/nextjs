import React, { PureComponent } from 'react'
// import PropTypes from 'prop-types';

import { HeaderSection } from '../../sections'

class HeaderContainer extends PureComponent {
  static async getInitialProps () {}

  render () {
    return <HeaderSection />
  }
}

HeaderContainer.propTypes = {}

HeaderContainer.defaultProps = {}

export default HeaderContainer
