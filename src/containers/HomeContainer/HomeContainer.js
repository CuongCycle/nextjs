import React, { PureComponent } from 'react'
import Router from 'next/router'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
// import PropTypes from 'prop-types';

import Constants from '../../constants'
import { withTranslation } from '../../libs/i18n'

class HomeContainer extends PureComponent {
  static async getInitialProps(ctx) {
    const { store } = ctx
    const namespacesRequired = ['common']

    const doAllDispatches = (dispatch) => Promise.all([])

    try {
      await store.dispatch(doAllDispatches)
      return {
        namespacesRequired,
      }
    } catch (err) {
      console.error(err)

      return {
        namespacesRequired,
      }
    }
  }

  handleOnClick = () => {
    Router.push(Constants.PAGE.USERS)
  }

  render() {
    return (
      <>
        <Helmet>
          <title>Home Page</title>
          <meta name="description" content="Description of Home Page" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="Home Page" />
          <meta property="og:description" content="Description of Home Page" />
        </Helmet>
        <div className="s-section s-home">
          <div className="b-container">
            <h1>Home Page</h1>
            <button className="btn" onClick={this.handleOnClick}>
              Users <i className="icon icon-hotline" />
            </button>
          </div>
        </div>
      </>
    )
  }
}

const mapStateToProps = ({ common }) => {
  // const {} = common
  return {}
}

HomeContainer.propTypes = {}

HomeContainer.defaultProps = {}

export default connect(mapStateToProps, {})(withTranslation('common')(HomeContainer))
