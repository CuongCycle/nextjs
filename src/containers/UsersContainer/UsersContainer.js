import React, { PureComponent } from 'react'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { withTranslation } from '../../libs/i18n'
import { getUsers } from '../../actions'
import { UsersList } from '../../components'

class UsersContainer extends PureComponent {
  static async getInitialProps(ctx) {
    const { store } = ctx
    const namespacesRequired = ['common']

    const doAllDispatches = (dispatch) => Promise.all([dispatch(getUsers())])

    try {
      await store.dispatch(doAllDispatches)
      return {
        namespacesRequired,
      }
    } catch (err) {
      console.error(err)

      return {
        namespacesRequired,
      }
    }
  }

  render() {
    const { usersList } = this.props

    return (
      <>
        <Helmet>
          <title>Users</title>
          <meta name="description" content="Users List" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="Users" />
          <meta property="og:description" content="Users List" />
        </Helmet>
        <div className="s-section s-users">
          <div className="b-container">
            <h1>Users</h1>
            <UsersList {...usersList} />
          </div>
        </div>
      </>
    )
  }
}

const mapStateToProps = ({ common }) => {
  const { usersList } = common

  return { usersList }
}

UsersContainer.propTypes = {
  usersList: PropTypes.object,
}

UsersContainer.defaultProps = {
  usersList: {},
}

export default connect(mapStateToProps, {})(withTranslation('common')(UsersContainer))
