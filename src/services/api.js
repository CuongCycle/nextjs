const NODE_ENV = process.env.ENV
const apiProps = {
  baseURL: '',
  token: '',
}

if (NODE_ENV === 'production') {
  apiProps.baseURL = 'https://reqres.in/api/'
} else if (NODE_ENV === 'staging') {
  apiProps.baseURL = 'https://reqres.in/api/'
} else if (NODE_ENV === 'development') {
  apiProps.baseURL = 'https://reqres.in/api/'
} else {
  apiProps.baseURL = 'https://reqres.in/api/'
}

export const cAPI = {
  baseURL: apiProps.baseURL,
  headers: {
    Authorization: apiProps.token,
    'Content-Type': 'application/json',
  },
  timeout: 30000,
}
