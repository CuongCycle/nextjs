import React from 'react'
import * as R from 'ramda'
import App from 'next/app'
import Head from 'next/head'
// import cookie from 'js-cookie'
import NProgress from 'nprogress'
// import nextCookie from 'next-cookies'
import { Provider } from 'react-redux'
import withRedux from 'next-redux-wrapper'
import absoluteUrl from 'next-absolute-url'
import Router, { withRouter } from 'next/router'

import Layout from '../src/components/Layout'
import createStore from '../src/store/createStore'
import { appWithTranslation } from '../src/libs/i18n'
import { convertRouterToClassName } from '../src/utils/common'

Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    const { store, req } = ctx
    const namespacesRequired = ['common']
    const { origin } = absoluteUrl(req)
    // const { token } = nextCookie(ctx)
    // const getState = store.getState()
    // const {
    //   common: {},
    // } = getState
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {}
    const doAllDispatches = (dispatch) => Promise.all([])

    try {
      await store.dispatch(doAllDispatches)

      return {
        host: origin,
        pageProps: { ...pageProps, namespacesRequired },
      }
    } catch (err) {
      console.error(err)

      return {
        apiError: R.pathOr('', ['response', 'data'])(err),
        host: origin,
        pageProps: { ...pageProps, namespacesRequired },
      }
    }
  }

  render() {
    const { Component, pageProps, host, store, router } = this.props
    const layoutProps = {
      className: `p-${convertRouterToClassName(router)}`,
      host,
      router,
    }
    const componentProps = {
      host,
      pageProps,
      router,
    }

    return (
      <>
        <Head>
          <meta name="viewport" content="width=device-width,maximum-scale=1,initial-scale=1" />
        </Head>
        <Provider store={store}>
          <Layout {...layoutProps}>
            <Component {...componentProps} />
          </Layout>
        </Provider>
      </>
    )
  }
}

export default withRedux(createStore)(withRouter(appWithTranslation(MyApp)))
